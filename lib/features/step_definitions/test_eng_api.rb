Given(/^I claimed the challenge$/) do
  @api = TestEngAPI.new
  @result = @api.claim_challenge
end

Given(/^I followed the first step successfully$/) do
  @result = @api.first_step
end

Given(/^I followed the second step successfully$/) do
  @result = @api.second_step
end

When(/^I post the last step$/) do
  @result = @api.last_step
end

Then(/^I should receive a success message$/) do
  if @api.message.include?("Server Error")
	raise Exception.new("POST to /last_step failed")
  end
end

When(/^I request the second step$/) do
  @result = @api.second_step
end

Then(/^I should receive a message saying the request miss the key$/) do
  if not (@api.message.include?("Bad key!"))
	raise Exception.new("Incorrect message")
  end
end