

Given(/^I am on forgot password page$/) do
  @forgot_password_page = ForgotPasswordPage.new (get_new_browser)
  @forgot_password_page.goto
end

When /^I retrieve the password to an email address$/ do
	@temp_email_page = TempEmailPage.new (get_new_browser)
	@temp_email_page.goto
	@forgot_password_page.set_email (@temp_email_page.get_temp_email)
	@forgot_password_page.retrieve_password
end

Then /^I should receive a forgot password email$/ do
	@temp_email_page.is_email_visible
end