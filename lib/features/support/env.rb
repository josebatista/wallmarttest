require "bundler/setup"
Bundler.require (:default)

require 'capybara'
require 'capybara/dsl'
require_relative '../../test_eng_api.rb'

module BrowserFactory
	include Capybara::DSL
	
	def get_new_browser
		Capybara.run_server = :run_server_state
		browser = Capybara::Session.new (:selenium)
		return browser
	end
	
end

World (BrowserFactory)