Feature: Challenge API


	Scenario: Get success message at last step
	Given I claimed the challenge
	And I followed the first step successfully
	And I followed the second step successfully
	When I post the last step
	Then I should receive a success message

	
	Scenario: Follow to second step without first step
	Given I claimed the challenge
	When I request the second step
	Then I should receive a message saying the request miss the key