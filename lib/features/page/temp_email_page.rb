require "bundler/setup"
Bundler.require (:default)

require 'capybara'
require 'capybara/dsl'

class TempEmailPage
	
	
	def initialize (session)
		@session = session
	end

	def goto ()
		@session.visit ("https://temp-mail.org/pt/")
	end
	
	def get_temp_email ()
		@session.find(:id, "mail").value
	end

	def refresh ()
		@session.find(:id, "click-to-refresh").click()
	end
	
	def has_email ()
		begin
			result = @session.all(:xpath, "//table[@id='mails']//descendant::tbody//td")
			if result.count() > 0 
				return true
			else 
				return false
			end
		rescue Capybara::ElementNotFound
			return false
		end
	end
	
	def is_email_visible ()
		while has_email!=true
			refresh()
		end
		return true
	end
	
end