require "bundler/setup"
Bundler.require (:default)

require 'capybara'
require 'capybara/dsl'

class ForgotPasswordPage 

	def initialize (session)
		@session = session
	end
	
	def goto 
		@session.visit("https://the-internet.herokuapp.com/forgot_password")
	end
	
	def set_email (email)
		@session.find(:id, "email").send_keys (email)
	end
	
	def retrieve_password ()
		@session.find(:id,"form_submit").click
	end
	
end