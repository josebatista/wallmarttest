require "bundler/setup"
Bundler.require (:default)


class TestEngAPI
	include HTTParty
	
	base_uri "https://test-eng-api.herokuapp.com"
	
	attr_accessor :token, :key, :hash, :path, :message
	
	
	
	def claim_challenge 
		result = self.class.get("/claim_challenge", 
			:verify => false,
			:headers => {"Accept" => "application/json"})
		self.token = result["token"]
		self.path = result ["path"]
		self.message = result ["message"]
		return result
	end
	
	
	def first_step		
		result_temp =self.class.get("/first_step", 
			:verify => false,
			:headers => {"Accept" => "application/json", :token => "#{token}"} )
		result = JSON.parse (result_temp)
		self.path=result["path"]
		self.message = result["message"]
		if match = "#{message}".match(/firstKey[^\w]*(.*)/)
			self.key = match.captures.first
		end
		return result
	end

	
	def second_step
		result_temp = self.class.get("/second_step", 
			:query => {"firstKey" => "#{key}"},
			:verify => false,
			:headers => {"Accept" => "application/json", :token => "#{token}"} )
		begin 
			result = JSON.parse (result_temp)
			self.path = result["path"]
			self.message = result["message"]
			return result
		rescue
			self.message = result_temp
			return result_temp
		end
	end
	
	def last_step
		result_temp = self.class.post("/last_step",
			:verify => false,		
			:query => {"firstKey" => "#{key}", :token => "#{token}"},
			:body => {"firstKey" =>  "#{key}", :token => "#{token}"}.to_json,
			:headers => {"Accept" => "application/json", "Content-Type" => "application/json" , :token => "#{token}"} )
		begin 
			result = JSON.parse (result_temp)
			self.message = result["message"]
			return result
		rescue
			self.message = result_temp
			return result_temp
		end
	end
end	
