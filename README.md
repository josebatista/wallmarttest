# README #

Esse � o projeto de automa��o para o processo de sele��o.
O projeto foi realizado com Ruby, Capybara, HTTParty e Cucumber.

### Pr�-Requisitos ###
As seguintes instala��es s�o necess�rias para o funcionamento do projeto:

* Ruby 	   (2.3.3p222)
* cucumber (2.4.0)
* httparty (0.15.5)
* capybara (2.14.4)
* bundler  (1.15.1)
* selenium-webdriver (2.53.0)
* Firefox  (33.0)

### Como executar ###

* ##### Instalando Bundles #####

Uma vez que tenha o ruby e o bundler instalados, basta baixar as demais dependencias a partir do pr�prio bundler.
Para isso � necess�rio abrir o prompt na pasta ``/wallmart`` e executar ``bundler install``

* ##### Executar #####

Como os cen�rios foram feitos com cucumber, foi criada uma feature para cada um dos exec�cios. Ambos os arquivos encontram-se em [lib/features](lib/features).

Para executar todos os cen�rios basta navegar at� a pasta [lib](lib) e executar o comando ``cucumber``.

Caso precise executar uma feature em espec�fico � necess�rio  navegar at� a pasta [lib](lib) e executar o comando ``cucumber features/myfeaturefile.feature``.